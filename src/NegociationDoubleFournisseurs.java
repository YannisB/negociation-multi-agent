import Models.Billet;
import Models.Fournisseur;
import Models.Negociateur;

import java.util.Date;

public class NegociationDoubleFournisseurs {
    public static void main(String[] args) {
        Billet billetLondresNewYork = new Billet("Londres", "NewYork", new Date(2019, 01, 25, 16, 30), new
                Date(2019, 01, 30, 10, 00));

        Fournisseur airFrance = new Fournisseur("Air France", 0.5);
        Fournisseur usa_airline = new Fournisseur("USA Airline", 0.75);
        usa_airline.proposeBillet(billetLondresNewYork, 400);
        airFrance.proposeBillet(billetLondresNewYork, 330);

        Negociateur paul = new Negociateur("Paul", "\033[0;34m"); // Négociations en Bleu
        paul.getFournisseurs().add(airFrance);
        paul.getFournisseurs().add(usa_airline);

        paul.debutNegociation(billetLondresNewYork, 330);
    }
}
