package Models;

import java.util.Date;

public class Message {
    private Agent emetteur;
    private Agent destinataire;
    private Billet billet;
    private double prixPropose;
    public static final int DUREE = 500;
    private Date dateEnvoi;

    public Message(Agent emetteur, Agent destinataire, Billet billet, double prixPropose, Date dateEnvoi) {
        this.emetteur = emetteur;
        this.destinataire = destinataire;
        this.billet = billet;
        this.prixPropose = prixPropose;
        this.dateEnvoi = dateEnvoi;
    }

    public Message (Message message) {
        this.emetteur = message.getEmetteur();
        this.destinataire = message.getDestinataire();
        this.billet = message.getBillet();
        this.prixPropose = message.getPrixPropose();
        this.dateEnvoi = message.getDateEnvoi();
    }

    //region Getters-Setters
    public Agent getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(Agent emetteur) {
        this.emetteur = emetteur;
    }

    public Agent getDestinataire() {
        return destinataire;
    }

    public void setDestinataire(Agent destinataire) {
        this.destinataire = destinataire;
    }

    public Billet getBillet() {
        return billet;
    }

    public void setBillet(Billet billet) {
        this.billet = billet;
    }

    public Date getDateEnvoi() {
        return dateEnvoi;
    }

    public void setDateEnvoi(Date dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
    }

    public double getPrixPropose() {
        return prixPropose;
    }

    public void setPrixPropose(double prixPropose) {
        this.prixPropose = prixPropose;
    }
    //endregion
}
