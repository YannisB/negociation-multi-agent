package Models;

import java.util.Date;

public class Negociation implements Runnable {
    private Fournisseur fournisseur;
    private Negociateur negociateur;
    private Billet billet;
    private double prixInitialFournisseur;
    private double budgetNegociateur;
    private double prixFournisseur;
    private double prixNegociateur;
    private int iteration;
    private int nbMaxIterations;
    private EtatNegociation etat;
    private String color;

    public Negociation(Fournisseur fournisseur, Negociateur negociateur, Billet billet, int nbMaxIterations, double budgetNegociateur) {
        this.fournisseur = fournisseur;
        this.negociateur = negociateur;
        this.billet = billet;
        this.nbMaxIterations = nbMaxIterations;
        this.budgetNegociateur = budgetNegociateur;
        this.iteration = 0;
        this.prixNegociateur = this.budgetNegociateur;
        this.color = this.negociateur.getColor();
    }

    //region Getters-Setters

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Negociateur getNegociateur() {
        return negociateur;
    }

    public void setNegociateur(Negociateur negociateur) {
        this.negociateur = negociateur;
    }

    public Billet getBillet() {
        return billet;
    }

    public void setBillet(Billet billet) {
        this.billet = billet;
    }

    public double getPrixInitialFournisseur() {
        return prixInitialFournisseur;
    }

    public void setPrixInitialFournisseur(double prixInitialFournisseur) {
        this.prixInitialFournisseur = prixInitialFournisseur;
    }

    public double getBudgetNegociateur() {
        return budgetNegociateur;
    }

    public void setBudgetNegociateur(double budgetNegociateur) {
        this.budgetNegociateur = budgetNegociateur;
    }

    public double getPrixFournisseur() {
        return prixFournisseur;
    }

    public void setPrixFournisseur(double prixFournisseur) {
        this.prixFournisseur = prixFournisseur;
    }

    public double getPrixNegociateur() {
        return prixNegociateur;
    }

    public void setPrixNegociateur(double prixNegociateur) {
        this.prixNegociateur = prixNegociateur;
    }

    public int getNbMaxIterations() {
        return nbMaxIterations;
    }

    public void setNbMaxIterations(int nbMaxIterations) {
        this.nbMaxIterations = nbMaxIterations;
    }

    public int getIteration() {
        return iteration;
    }

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    public EtatNegociation getEtat() {
        return etat;
    }

    public void setEtat(EtatNegociation etat) {
        this.etat = etat;
    }
    //endregion

    @Override
    public String toString() {
        return "\n\tItération : " + iteration +
                "\n\tPrix fournisseur : " + this.prixFournisseur + "\n\tPrix négociateur" +
                " : " + this.prixNegociateur;
    }

    public boolean invalide() {
        return getIteration() >= getNbMaxIterations();
    }

    @Override
    public void run() {
        fournisseur.getMessagerie().send(new Message(negociateur, fournisseur, billet, 0, new Date()));
        this.etat = EtatNegociation.ATTENTE_FOURNISSEUR;

        while (true) {
            if (this.etat == EtatNegociation.ATTENTE_FOURNISSEUR) {
                if (this.fournisseur.getMessagerie().getMessages().size() > 0) {
                    traiterMessage(this.fournisseur.getMessagerie().getMessage(this.negociateur));
                    this.fournisseur.getMessagerie().delete(this.fournisseur.getMessagerie().getMessage(this.negociateur));
                }
            } else if (this.etat == EtatNegociation.ATTENTE_NEGOCIATEUR) {
                if (this.negociateur.getMessagerie().getMessages().size() > 0) {
                    traiterMessage(this.negociateur.getMessagerie().getMessage(this.fournisseur));
                    this.negociateur.getMessagerie().delete(this.negociateur.getMessagerie().getMessage(this.fournisseur));
                }
            } else if (this.etat == EtatNegociation.ACCEPTE) {
                System.out.println(this.getColor() + "Négociation acceptée ! Prix : " + this.prixFournisseur);
                Thread.currentThread().stop();
            } else {
                System.out.println(this.getColor() + "Négociation refusée !");
                Thread.currentThread().stop();
            }
        }
    }

    private void traiterMessage(Message message) {
        if (invalide() || message.getBillet() == null) {
            refuserProposition();
        } else {
            if (this.iteration < 2)
                fixerPrix(message);
            affichageIteration(message);

            if (this.etat == EtatNegociation.ATTENTE_NEGOCIATEUR && this.iteration > 0) {
                // Negociateur
                double nouveauPrix = this.negociateur.variationPrix(this.prixNegociateur, this.prixFournisseur, this.budgetNegociateur);
                if (okNegociateur(nouveauPrix))
                    accepterProposition(message.getPrixPropose());
                else if (nouveauPrix > budgetNegociateur)
                    refuserProposition();
                else {
                    this.etat = EtatNegociation.ATTENTE_FOURNISSEUR;
                    this.prixNegociateur = nouveauPrix;
                    this.negociateur.getMessagerie().send(new Message(negociateur, fournisseur, this.billet, nouveauPrix, new Date
                            ()));
                }
            } else if (this.etat == EtatNegociation.ATTENTE_FOURNISSEUR && this.iteration > 0) {
                // Fournisseur
                double nouveauPrix = this.fournisseur.variationPrix(message.getPrixPropose(), this.prixFournisseur, 0);
                if (okFournisseur(nouveauPrix))
                    accepterProposition(message.getPrixPropose());
                else if (this.fournisseur.verificationPrixVente(this.prixInitialFournisseur, nouveauPrix))
                    refuserProposition();
                else {
                    this.etat = EtatNegociation.ATTENTE_NEGOCIATEUR;
                    this.prixFournisseur = nouveauPrix;
                    this.fournisseur.getMessagerie().send(new Message(fournisseur, negociateur, this.billet, nouveauPrix, new Date
                            ()));
                }
            }
            this.iteration++;
        }
    }

    private boolean okFournisseur(double nouveauPrix) {
        return (nouveauPrix < this.prixNegociateur || prixInitialFournisseur < this.prixNegociateur) && this.iteration > 0;
    }

    private boolean okNegociateur(double nouveauPrix) {
        return (budgetNegociateur > this.prixFournisseur || nouveauPrix > this.prixFournisseur);
    }

    private void accepterProposition(double prixPropose) {
        this.etat = EtatNegociation.ACCEPTE;

        this.prixFournisseur = prixPropose;
        this.prixNegociateur = prixPropose;
    }

    private void refuserProposition() {
        this.etat = EtatNegociation.REFUSE;
    }

    private void affichageIteration(Message message) {
        if (iteration == 0)
            premiereIteration(message);
        else {
            if (iteration == 1)
                System.out.println(this.getColor() + "[Fournisseur - " + fournisseur.getNom() + "] Billet proposé : " + message.getBillet() + "\n au prix de : " + message.getPrixPropose());
            System.out.println(message.getDestinataire() instanceof Negociateur ? this.getColor() + "[" + this.iteration + " - Negociateur - " + negociateur.getNom() + "] Proposition reçue : " + message.getPrixPropose() :
                    this.getColor() + "[" + this.iteration + " - Fournisseur - " + fournisseur.getNom() + "] Proposition reçue : " + message.getPrixPropose());
        }
    }

    private void fixerPrix(Message message) {
        if (this.etat == EtatNegociation.ATTENTE_NEGOCIATEUR)
            this.prixFournisseur = message.getPrixPropose();
        else if (this.etat == EtatNegociation.ATTENTE_FOURNISSEUR)
            this.prixNegociateur = message.getPrixPropose();
    }

    private void premiereIteration(Message message) {
        System.out.println(this.getColor() + "[Négociateur - " + negociateur.getNom() + "] Billet demandé : " + message.getBillet() +
                "\npour un budget de " + this.budgetNegociateur);

        this.billet = this.fournisseur.possedeBilletDemande(message.getBillet());

        if (this.billet == null) {
            System.out.println(this.getColor() + "[Fournisseur - " + fournisseur.getNom() + "] Billet non possédé !");
            this.etat = EtatNegociation.REFUSE;
        } else {
            fournisseur.getMessagerie().send(new Message(fournisseur, negociateur, this.billet, this.billet.getPrix(), new Date
                    ()));
            this.prixFournisseur = this.billet.getPrix();
            this.prixInitialFournisseur = this.prixFournisseur;
            this.etat = EtatNegociation.ATTENTE_NEGOCIATEUR;
        }
    }
}
