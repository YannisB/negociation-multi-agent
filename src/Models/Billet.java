package Models;

import java.util.Date;

public class Billet {
    private String source;
    private String destination;
    private Date depart;
    private Date arrivee;
    private double prix;

    public Billet(String source, String destination, Date depart, Date arrivee) {
        this.source = source;
        this.destination = destination;
        this.depart = depart;
        this.arrivee = arrivee;
        this.prix = 0;
    }

    public Billet(Billet billet, double prix) {
        this.source = billet.getSource();
        this.destination = billet.getDestination();
        this.depart = billet.getDepart();
        this.arrivee = billet.getArrivee();
        this.prix = prix;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDepart() {
        return depart;
    }

    public void setDepart(Date depart) {
        this.depart = depart;
    }

    public Date getArrivee() {
        return arrivee;
    }

    public void setArrivee(Date arrivee) {
        this.arrivee = arrivee;
    }

    @Override
    public boolean equals(Object obj) {
        Billet billetDemande = (Billet) obj;

        return (this.getSource().equals(billetDemande.getSource())
                && this.getDestination().equals(billetDemande.getDestination())
                && this.getDepart().equals(billetDemande.getDepart())
                && this.getArrivee().equals(billetDemande.getArrivee()));
    }

    @Override
    public String toString() {
        return "\n\tSource : " + this.source + "\n\tDestinataire : " + this.destination + "\n\tDepart : " + this.depart +
                "\n\tArrivée : " + this.arrivee;
    }
}
