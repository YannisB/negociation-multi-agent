package Models;

import java.util.ArrayList;
import java.util.List;

public class Negociateur extends Agent {

    private String nom;
    private List<Fournisseur> fournisseurs;
    private String color;

    public Negociateur(String nom, String color) {
        this.nom = nom;
        this.fournisseurs = new ArrayList<>();
        this.color = color;
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void debutNegociation(Billet billetVoulu, double budget) {

        if (billetVoulu != null) {
            for (Fournisseur fournisseur : getFournisseurs()) {
                System.out.println("Envoi à " + fournisseur.getNom());
                new Thread(new Negociation(fournisseur, this, billetVoulu, 20, budget)).start();
            }
        }
    }
}
