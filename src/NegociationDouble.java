import Models.Billet;
import Models.Fournisseur;
import Models.Negociateur;

import java.util.Date;

public class NegociationDouble {
    public static void main(String[] args) {
        Billet billetParisMadrid = new Billet("Paris", "Madrid", new Date(2019, 01, 25, 16, 30), new
                Date(2019, 01, 30, 10, 00));

        Fournisseur airFrance = new Fournisseur("Air France", 0.5);
        Fournisseur maroccoAirline = new Fournisseur("Marocco Airline", 0.75);
        maroccoAirline.proposeBillet(billetParisMadrid, 400);
        airFrance.proposeBillet(billetParisMadrid, 330);

        Negociateur jean = new Negociateur("Jean", "\033[0;34m"); // Négociations en Bleu
        Negociateur paul = new Negociateur("Paul", "\033[0;32m"); // Négociations en Jaune
        paul.getFournisseurs().add(airFrance);
        jean.getFournisseurs().add(airFrance);
        paul.getFournisseurs().add(maroccoAirline);
        jean.getFournisseurs().add(maroccoAirline);

        paul.debutNegociation(billetParisMadrid, 330);
        jean.debutNegociation(billetParisMadrid, 150);
    }
}
