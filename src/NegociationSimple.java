import Models.Billet;
import Models.Fournisseur;
import Models.Negociateur;

import java.util.Date;

public class NegociationSimple {
    public static void main(String [] args) {
        Billet billetParisMadrid = new Billet("Paris", "Madrid", new Date(2019, 01, 25, 16, 30), new
                Date(2019, 01, 30, 10, 00));

        Fournisseur airFrance = new Fournisseur("Air France", 0.50);
        airFrance.proposeBillet(billetParisMadrid, 330);

        Negociateur jean = new Negociateur("Jean", "");
        jean.getFournisseurs().add(airFrance);

        jean.debutNegociation(billetParisMadrid, 280);
    }
}
