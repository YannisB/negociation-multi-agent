package Models;

import java.util.ArrayList;
import java.util.List;

public class Fournisseur extends Agent {
    private String nom;
    private List<Billet> billets;
    private Messagerie messagerie;
    private double pourcentage;

    public Fournisseur(String nom, double pourcentage) {
        this.nom = nom;
        this.pourcentage = pourcentage;
        this.billets = new ArrayList<>();
        this.messagerie = new Messagerie();
    }

    public void proposeBillet (Billet billet, double prix) {
        Billet billetAvecPrixFixe = new Billet(billet, prix);

        billets.add(billetAvecPrixFixe);
    }

    //region Getters-Setters
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Billet> getBillets() {
        return billets;
    }

    public Messagerie getMessagerie() {
        return messagerie;
    }

    public void setMessagerie(Messagerie messagerie) {
        this.messagerie = messagerie;
    }

    public double getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(double pourcentage) {
        this.pourcentage = pourcentage;
    }

    //endregion

    public boolean estLeBilletDemande (Billet billet, Billet billetDemande) {
        return (billet.getSource().equals(billetDemande.getSource())
                && billet.getDestination().equals(billetDemande.getDestination())
                && billet.getDepart().equals(billetDemande.getDepart())
                && billet.getArrivee().equals(billetDemande.getArrivee()));
    }

    public Billet possedeBilletDemande(Billet billet) {
        for(Billet b : this.getBillets()) {
            if(estLeBilletDemande(b, billet))
                return b;
        }
        return null;
    }

    public boolean verificationPrixVente(double prixinitial, double nouveauprix) {
        if (nouveauprix <= prixinitial * pourcentage)
            return true;
        return false;
    }
}
