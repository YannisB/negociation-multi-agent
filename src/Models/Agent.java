package Models;

public class Agent {
    private Messagerie messagerie;

    public Agent() {
        this.messagerie = new Messagerie();
    }

    public Messagerie getMessagerie() {
        return messagerie;
    }

    public void setMessagerie(Messagerie messagerie) {
        this.messagerie = messagerie;
    }

    public double variationPrix(double prixN, double prixF, double budgetNeg) {
        if (budgetNeg == 0) {
            return prixF - ((prixN / prixF) / 20) * prixN;
        }
        else {
            return prixN > 0 ? prixN + ((prixN / prixF) / 15) * prixF : (budgetNeg > prixF / 2 ? prixF / 2 : budgetNeg / 2);
        }
    }
}
