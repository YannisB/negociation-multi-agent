package Models;

import java.util.ArrayList;
import java.util.List;

public class Messagerie {
    private List<Message> messages;

    public Messagerie() {
        this.messages = new ArrayList<>();
    }

    public List<Message> getMessages() {
        return messages;
    }

    public Message getMessage(Agent agentEnContact) {
        for (Message message : messages) {
            if (message.getEmetteur() == agentEnContact) {
                return message;
            }
        }

        return null;
    }

    public synchronized void send(Message message) {
        Agent agent = message.getDestinataire();
        agent.getMessagerie().getMessages().add(message);
    }

    public synchronized void delete(Message message) {
        this.messages.remove(message);
    }
}
