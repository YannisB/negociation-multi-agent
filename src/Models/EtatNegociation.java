package Models;

public enum EtatNegociation {
    ACCEPTE, REFUSE, ATTENTE_NEGOCIATEUR, ATTENTE_FOURNISSEUR;
}
